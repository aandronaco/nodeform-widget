import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {NodeForm} from "./model/nodeform.model";

@Injectable({
  providedIn: 'root'
})
export class NodeformService {

  constructor(private http: HttpClient) { }



  createNode(node: NodeForm) {
    return this.http.post('api/node', node)
        .toPromise().catch(this.handleErrorPromise);
  }

  editNode(node: NodeForm) {
    return this.http.put('api/node', node)
      .toPromise().catch(this.handleErrorPromise);
  }

  retrieveNode(nodeId: number){
    return this.http.get('api/node/' + nodeId)
      .toPromise().catch(this.handleErrorPromise);
  }

  retrieveNodeTree(nodeId: number) {
    return this.http.get('api/nodeTree/' + nodeId)
      .toPromise().catch(this.handleErrorPromise);
  }

  private handleErrorPromise(error: any) {
    return Promise.reject(error);
  }



}
