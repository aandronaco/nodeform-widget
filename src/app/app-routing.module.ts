import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {NewNodeComponent} from "./new-node/new-node.component";
import {EditNodeComponent} from "./edit-node/edit-node.component";
import {TreeNodeComponent} from "./tree-node/tree-node.component";

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'new-node', component: NewNodeComponent },
  { path: 'edit-node', component: EditNodeComponent },
  { path: 'tree-node', component: TreeNodeComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' } // default route

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
