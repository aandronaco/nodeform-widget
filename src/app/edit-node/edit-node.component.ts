import { Component, OnInit } from '@angular/core';
import {NodeForm} from "../model/nodeform.model";
import {NodeformService} from "../nodeform.service";
import {Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-edit-node',
  templateUrl: './edit-node.component.html',
  styleUrls: ['./edit-node.component.scss']
})
export class EditNodeComponent implements OnInit {

  nodeForm: NodeForm = {} as NodeForm;
  message: string = '';
  success: boolean=false;
  nodeId: number;
  retrieveSuccess: boolean = false;
  retrieveMessage: string = '';
  idNodeFormGroup: FormGroup;
  maxInteger: number= Number.MAX_SAFE_INTEGER;

  constructor( private router: Router, private nodeFormService : NodeformService) {
    this.idNodeFormGroup = new FormGroup({
      idNode: new FormControl(this.nodeId,
        [
          Validators.required,
          Validators.pattern("^[0-9]*$")
        ])
    });
  }
  get idNode() { return this.idNodeFormGroup.get('idNode');}

  ngOnInit(): void {
  }

  editNode() {
    console.log("node is", this.nodeForm );
    this.nodeFormService.editNode(this.nodeForm).then(data => {
        this.success = true;
        this.message = 'Node edited successfully!';
      },
      err => {
        this.success = false;
        this.message = 'Error: ' + err.error.message;
      });
  }

  retrieveNode() {
    this.idNode?.markAllAsTouched();
    if(this.idNode?.valid) {
      console.log("idNode is", this.idNode?.value)

      this.nodeFormService.retrieveNode(this.idNode?.value).then(data => {
          this.retrieveSuccess = true;
          this.retrieveMessage = 'Node retrieved successfully!';
          this.nodeForm = data as NodeForm;
        },
        err => {
          this.retrieveSuccess = false;
          this.retrieveMessage = 'Error: ' + err.error.message;
        });
    }
  }

  goToHome() {
    this.router.navigate(['']);
  }
}
