import { Component, OnInit } from '@angular/core';
import {NodeForm} from "../model/nodeform.model";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {NodeformService} from "../nodeform.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-new-node',
  templateUrl: './new-node.component.html',
  styleUrls: ['./new-node.component.scss']
})
export class NewNodeComponent implements OnInit {

  nodeForm: NodeForm = {} as NodeForm;
  nodeFormGroup: FormGroup;

  nameMaxLength: number= 50;
  descriptionMaxLength: number= 250;

  message: string='';
  success: boolean = false;
  maxInteger: number = Number.MAX_SAFE_INTEGER;

  constructor( private router: Router, private nodeFormService : NodeformService) {
    this.nodeFormGroup = new FormGroup({
      name: new FormControl(this.nodeForm.name,
        [
          Validators.required,
          Validators.maxLength(this.nameMaxLength)
        ]),
      description :  new FormControl(this.nodeForm.description,
        [Validators.required, Validators.maxLength(this.descriptionMaxLength)]),
      parentId :  new FormControl(this.nodeForm.parent_id,
        [Validators.pattern("^[0-9]*$")])
    });
  }

  ngOnInit(): void {

  }

  get name() { return this.nodeFormGroup.get('name');}

  get description() { return this.nodeFormGroup.get('description');}

  get parentId() { return this.nodeFormGroup.get('parentId');}

  createNode() {
    this.message ='';
    this.nodeFormGroup.markAllAsTouched();
    if(this.nodeFormGroup.valid) {
      this.nodeForm.name = this.nodeFormGroup.get('name')?.value;
      this.nodeForm.description = this.nodeFormGroup.get('description')?.value;
      this.nodeForm.parent_id = this.nodeFormGroup.get('parentId')?.value;

      console.log("node is", this.nodeForm);
      this.nodeFormService.createNode(this.nodeForm).then(data => {
          this.success = true;
          this.message = 'Node created successfully with id: ' + data;

          // clean old data
          this.nodeForm={} as NodeForm;
          this.nodeFormGroup.reset();
        },
        err => {
          this.success = false;
          this.message = 'Error: ' + err.error.message;
        });
    }
  }

  goToHome() {
    this.router.navigate(['']);
  }
}
