export interface NodeForm {
  id: number;
  name: string;
  description: string;
  parent_id: number;
  nodes: Array<NodeForm>;
}
