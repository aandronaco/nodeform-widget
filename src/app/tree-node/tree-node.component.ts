import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {NodeformService} from "../nodeform.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {NodeForm} from "../model/nodeform.model";

@Component({
  selector: 'app-tree-node',
  templateUrl: './tree-node.component.html',
  styleUrls: ['./tree-node.component.scss']
})
export class TreeNodeComponent implements OnInit {
  maxInteger: number = Number.MAX_SAFE_INTEGER;
  idNodeFormGroup: FormGroup;
  nodeId: number;
  retrieveSuccess: boolean = false;
  retrieveMessage: string = '';
  nodeForm: NodeForm = {} as NodeForm;
  nodeTree: string='';


  constructor( private router: Router, private nodeFormService : NodeformService) {
    this.idNodeFormGroup = new FormGroup({
      idNode: new FormControl(this.nodeId,
        [
          Validators.required,
          Validators.pattern("^[0-9]*$")
        ])
    });
  }
  get idNode() { return this.idNodeFormGroup.get('idNode');}

  ngOnInit(): void {
  }



  retrieveNodeTree() {
    this.idNode?.markAllAsTouched();
    if(this.idNode?.valid) {
      console.log("idNode is", this.idNode?.value)

      this.nodeFormService.retrieveNodeTree(this.idNode?.value).then(data => {
          this.retrieveSuccess = true;
          this.retrieveMessage = 'Node retrieved successfully!';
          this.nodeForm = data as NodeForm;
          this.nodeTree=this.buildNodeTree(this.nodeForm);
        },
        err => {
          this.retrieveSuccess = false;
          this.retrieveMessage = 'Error: ' + err.error.message;
        });
    }
  }

  goToHome() {
    this.router.navigate(['']);
  }


  buildNodeTree(nodeForm: NodeForm): string {
    let textRow = '<ul>';
    textRow+= this.buildNodeFormRow(nodeForm);
    if(nodeForm.nodes  &&  nodeForm.nodes.length>0){
      for (let node of nodeForm.nodes) {
        textRow+=  this.buildNodeTree(node);
      }
    }
    textRow+='</ul>';

    return textRow;
  }

  buildNodeFormRow(nodeForm: NodeForm): string{
    let row =
      '<li>' +
          '<p>'+
            '<div>Node: ' + nodeForm.id + '</div>'+
            '<div>Name: ' + nodeForm.name + '</div>'+
            '<div>Description: ' + nodeForm.description + '</div>'+
            '<div>Parent: ' + nodeForm.parent_id + '</div>'+
          '</p>'+
      '</li>';
    return row;
  }

}
