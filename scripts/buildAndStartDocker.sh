docker build -f Dockerfile -t alfredo/nodeform-widget:latest .
docker run --add-host host.docker.internal:$(ip addr show docker0 | grep -Po 'inet \K[\d.]+') -p 8081:80 alfredo/nodeform-widget:latest
